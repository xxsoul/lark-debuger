package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"lark-debuger/enum"
	proto "lark-debuger/message"
	metadata "lark-debuger/message/metadata"
	simple "lark-debuger/message/simple"
	"net"
	"reflect"
	"strconv"
)

func main() {
	fmt.Println("hello word")
	data := getBytes()

	var dataBuf bytes.Buffer
	dataBuf.Write([]byte("SimpleRequest "))
	dataBuf.Write([]byte(strconv.Itoa(len(data))))
	dataBuf.Write([]byte("\r\n"))
	dataBuf.Write(data)

	con, err := net.DialTCP("tcp", nil, &net.TCPAddr{
		IP:   net.ParseIP("127.0.0.1"),
		Port: 9679})
	if err != nil {
		fmt.Printf("出错了" + err.Error())
		return
	}

	_, err2 := con.Write(dataBuf.Bytes())
	if err2 != nil {
		fmt.Printf("出错了2" + err2.Error())
		return
	}
	// 读取response
	var resBuf bytes.Buffer
	buf := make([]byte, 2560)
	for {
		n, err := con.Read(buf)
		resBuf.Write(buf[:n])
		if err != nil || n < 2560 {
			break
		}
	}

	// fmt.Println("---------------------------------------------")
	// fmt.Println(fmt.Sprintf("%b", resBuf.Bytes()))
	fmt.Println("---------------------------------------------")

	// 脱壳，暂时不考虑粘包等问题
	resBuf.Next(len([]byte("SimpleResponse ")))
	for b, rerr := resBuf.ReadByte(); rerr == nil; {
		if string(b) == "\r" {
			// 再读一个\n
			resBuf.ReadByte()
			break
		}
		b, rerr = resBuf.ReadByte()
	}

	// res.InitByByte(resBuf.Bytes())

	// resJByte, _ := json.Marshal(res)
	// fmt.Println(string(resJByte))
	// reqDataMsg := data
	// offest := 0

	var res simple.SimpleResponse
	res = proto.DecodePbMessageToObject(resBuf.Bytes(), reflect.TypeOf(res)).(simple.SimpleResponse)
	// resJByte, _ := json.Marshal(res)
	// fmt.Println(string(resJByte))

	serviceRes := decodeServiceResult(res.Result)
	sResJByte, _ := json.Marshal(serviceRes)
	fmt.Println("---------------------------------------------")
	fmt.Println(string(sResJByte))
}

func getBytes() []byte {
	req := &simple.SimpleRequest{
		ClientName:  "mtime.cinema.subsidy.service",
		ServiceName: "$meta",
		MethodName:  "GetService",
		ContextID:   "5d47ffdfcc391f36047100f9",
		MessageID:   "5d47ffe0cc391f36047100fa",
		ServerName:  "mtime.cinema.subsidy.service"}

	req.AddParameter(enum.SimpleType.DtString, []byte("ActivityService"))
	// return req.Encode()
	res, _ := proto.EncodeObjectToPbMessage(req)
	return res
}

// 解析Service信息
func decodeServiceResult(res simple.SimpleValue) metadata.ServiceInfo {
	var serviceInfo metadata.ServiceInfo
	serviceInfo = proto.DecodePbMessageToObject(res.Data, reflect.TypeOf(serviceInfo)).(metadata.ServiceInfo)
	return serviceInfo
}
