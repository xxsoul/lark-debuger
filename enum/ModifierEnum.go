package enum

// ModifierEnum 用于映射lark中，Modifier值的枚举
type ModifierEnum struct {
	Optional int
	Required int
	Repeated int
}

var (
	//Modifier 枚举值的指针
	Modifier = &ModifierEnum{Optional: 1, Required: 2, Repeated: 3}
)