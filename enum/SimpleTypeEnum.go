package enum

// SimpleTypeEnum 基本类型的枚举，对应SimpleValue中DataType字段的值，用于lark框架传递参数时使用
type SimpleTypeEnum struct {
	DtNull                  int
	DtByteArray             int
	DtString                int
	DtInt32                 int
	DtInt64                 int
	DtBool                  int
	DtFloat                 int
	DtDouble                int
	DtProtobuf              int
	DtBoolArray             int
	DtStringArray           int
	DtInt32Array            int
	DtInt64Array            int
	DtFloatArray            int
	DtDoubleArray           int
	DtCompressedBoolArray   int
	DtCompressedStringArray int
	DtCompressedInt32Array  int
	DtCompressedInt64Array  int
	DtCompressedFloatArray  int
	DtCompressedDoubleArray int
	DtCompressedProtobuf    int
	DtCompressedString      int
	DtCompressedByteArray   int
}

var (
	// SimpleType 枚举值的指针
	SimpleType = &SimpleTypeEnum{
		DtNull:                  0,
		DtByteArray:             1,
		DtString:                3,
		DtInt32:                 4,
		DtInt64:                 5,
		DtBool:                  10,
		DtFloat:                 18,
		DtDouble:                19,
		DtProtobuf:              21,
		DtBoolArray:             100,
		DtStringArray:           103,
		DtInt32Array:            104,
		DtInt64Array:            105,
		DtFloatArray:            118,
		DtDoubleArray:           119,
		DtCompressedBoolArray:   200,
		DtCompressedStringArray: 203,
		DtCompressedInt32Array:  204,
		DtCompressedInt64Array:  205,
		DtCompressedFloatArray:  218,
		DtCompressedDoubleArray: 219,
		DtCompressedProtobuf:    251,
		DtCompressedString:      254,
		DtCompressedByteArray:   255}
)
