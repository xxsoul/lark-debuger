package message

//SimpleValue rpc服务用于传递参数的结构
type SimpleValue struct {
	DataType int    `proto:"order=1,modifier=1"`
	Data     []byte `proto:"order=2,modifier=1"`
}

// // valueEncode 方法，用于对SimpleValue转化为字节流
// func (sv *SimpleValue) valueEncode() []byte {
// 	var buf bytes.Buffer

// 	buf.Write(EncodeInt(1, uint64(sv.DataType)))
// 	buf.Write(EncodeByte(2, sv.Data))

// 	return buf.Bytes()
// }

// // InitByByte 方法，用于将字节流转换回SimpleValue
// func (sv *SimpleValue) InitByByte(data []byte) {
// 	offest := uint64(0)
// 	for offest < uint64(len(data)) {
// 		svD, svO, _, _, svSz := DecodePbMessage(data[offest:])
// 		offest += svSz

// 		if svO == 1 {
// 			sv.DataType = int(DecodeInt(svD))
// 		} else if svO == 2 {
// 			sv.Data = svD
// 		}
// 	}
// }

// // DecodeData 方法，用于将SimpleValue解码为对象
// func (sv *SimpleValue) DecodeData() interface{} {
// 	switch sv.DataType {

// 	case enum.SimpleType.DtByteArray:
// 		return sv.Data

// 	case enum.SimpleType.DtString:
// 		return string(sv.Data)

// 	case enum.SimpleType.DtInt32:
// 		return int32(DecodeInt(sv.Data))

// 	case enum.SimpleType.DtInt64:
// 		return int64(DecodeInt(sv.Data))

// 	case enum.SimpleType.DtBool:
// 		return DecodeInt(sv.Data) > 0

// 	case enum.SimpleType.DtFloat:
// 		return DecodeFloat32(sv.Data)

// 	case enum.SimpleType.DtDouble:
// 		return DecodeFloat64(sv.Data)

// 	// 值是Protobuf message结构，按着message逐层解码
// 	case enum.SimpleType.DtProtobuf:
// 		return sv.decodeDtProtobufData(sv.Data)
// 	}

// 	return nil
// }

// // decodeDtProtobufData 方法，用于解析DtProtobuf的数据值，目前不考虑嵌套问题，直观输出
// func (sv *SimpleValue) decodeDtProtobufData(data []byte) map[int64]string {
// 	offest := 0
// 	res := make(map[int64]string)

// 	for offest < len(data) {
// 		d, o, t, _, ms := DecodePbMessage(data[offest:])
// 		offest += int(ms)

// 		//按tag解析，其实这里应该按order解析
// 		switch t {
// 		case 0:
// 			res[int64(o)] = string(DecodeInt(d))
// 		case 1:
// 			res[int64(o)] = strconv.FormatFloat(DecodeFloat64(d), 'E', -1, 64)
// 		case 2:
// 			res[int64(o)] = string(d)
// 		case 5:
// 			res[int64(o)] = strconv.FormatFloat(float64(DecodeFloat32(d)), 'E', -1, 32)
// 		}
// 	}

// 	return res
// }
