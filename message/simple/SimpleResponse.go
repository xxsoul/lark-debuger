package message

//SimpleResponse rpc请求的返回结构
type SimpleResponse struct {
	Success     bool         `proto:"order=1,modifier=1"`
	Result      SimpleValue  `proto:"order=2,modifier=2"`
	ErrorInfo   string       `proto:"order=3,modifier=2"`
	ServerTime  int64        `proto:"order=4,modifier=1"`
	ErrorCode   int          `proto:"order=6,modifier=2"`
	ErrorDetail string       `proto:"order=7,modifier=2"`
	MessageID   string       `proto:"order=8,modifier=2"`
}

// // InitByByte 方法，通过字节流初始化回SimpleResponse，由于本地不需要编码SimpleResponse，所以不需要Encode方法
// func (sr *SimpleResponse) InitByByte(data []byte) {
// 	offest := uint64(0)

// 	for offest < uint64(len(data)) {
// 		d, o, _, _, ms := DecodePbMessage(data[offest:])
// 		offest += ms
// 		if d == nil {
// 			break
// 		}

// 		switch o {
// 		case uint64(1):
// 			sr.Success = DecodeInt(d) > 0
// 		case uint64(2):
// 			sr.Result = new(SimpleValue)
// 			sr.Result.InitByByte(d)
// 		case uint64(3):
// 			sr.ErrorInfo = DecodeString(d)
// 		case uint64(4):
// 			sr.ServerTime = DecodeInt(d)
// 		case uint64(6):
// 			sr.ErrorCode = int(DecodeInt(d))
// 		case uint64(7):
// 			sr.ErrorDetail = DecodeString(d)
// 		case uint64(8):
// 			sr.MessageID = DecodeString(d)
// 		}
// 	}
// }
