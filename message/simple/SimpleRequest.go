package message

//SimpleRequest rpc服务的请求结构
type SimpleRequest struct {
	ClientName  string        `proto:"order=1,modifier=1"`
	UserToken   string        `proto:"order=2,modifier=2"`
	ServiceName string        `proto:"order=3,modifier=1"`
	MethodName  string        `proto:"order=4,modifier=1"`
	Parameters  []SimpleValue `proto:"order=5,modifier=3"`
	ContextID   string        `proto:"order=7,modifier=2"`
	MessageID   string        `proto:"order=8,modifier=2"`
	ServerName  string        `proto:"order=9,modifier=2"`
}

// // Encode 方法，用于对SimpleRequest转化为字节流，由于本地不需要解析SimpleRequest，所以不需要Decode方法
// func (sr *SimpleRequest) Encode() []byte {
// 	var buf bytes.Buffer
// 	buf.Write(EncodeString(1, sr.ClientName))
// 	buf.Write(EncodeString(3, sr.ServiceName))
// 	buf.Write(EncodeString(4, sr.MethodName))
// 	if len(sr.Parameters) > 0 {
// 		for _, sv := range sr.Parameters {
// 			buf.Write(EncodeByte(5, sv.valueEncode()))
// 		}
// 	}
// 	buf.Write(EncodeString(7, sr.ContextID))
// 	buf.Write(EncodeString(8, sr.MessageID))
// 	buf.Write(EncodeString(9, sr.ServerName))

// 	return buf.Bytes()
// }

// AddParameter 方法，向SimpleRequest中添加参数
// paramType 取值参考SimpleTypeEnum结构中取值
// paramData 为已经转换为字节流的参数信息
func (sr *SimpleRequest) AddParameter(paramType int, paramData []byte) {
	sv := SimpleValue{
		DataType: paramType,
		Data:     paramData}

	sr.Parameters = append(sr.Parameters, sv)
}
