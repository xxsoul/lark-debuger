package message

// create by xinlei.xiao 2019/10/12

// EnumInfo 用于描述枚举的信息
type EnumInfo struct {
	Name  string `proto:"order=1,modifier=2"`
	Value int    `proto:"order=2,modifier=2"`
}
