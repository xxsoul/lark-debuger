package message

// create by xinlei.xiao 2019/10/12

// TypeInfo 用于描述类型信息
// Kind 取值SimpleTypeEnum
type TypeInfo struct {
	ID          string      `proto:"order=1,modifier=2"`
	Name        string      `proto:"order=2,modifier=2"`
	Kind        int         `proto:"order=3,modifier=2"`
	Description string      `proto:"order=4,modifier=1"`
	Fields      []FieldInfo `proto:"order=5,modifier=1"`
	Enums       []EnumInfo  `proto:"order=6,modifier=1"`
}
