package message

// create by xinlei.xiao 2019/10/12

// ServiceInfo 描述服务信息
type ServiceInfo struct {
	Name        string       `proto:"order=1,modifier=2"`
	Description string       `proto:"order=2,modifier=1"`
	Methods     []MethodInfo `proto:"order=3,modifier=3"`
}

// ServiceListInfo 描述服务列表信息
type ServiceListInfo struct {
	Services []ServiceInfo `proto:"order=1,modifier=3"`
}
