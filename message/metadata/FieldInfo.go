package message

// create by xinlei.xiao 2019/10/12

// FieldInfo 描述字段信息
type FieldInfo struct {
	Name        string   `proto:"order=1,modifier=2"`
	Type        TypeInfo `proto:"order=2,modifier=2"`
	Description string   `proto:"order=3,modifier=1"`
	Order       int      `proto:"order=4,modifier=2"`
	Modifier    int      `proto:"order=5,modifier=2"`
	Kind        int      `proto:"order=6,modifier=2"`
}
