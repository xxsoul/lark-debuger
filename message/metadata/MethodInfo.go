package message

// create by xinlei.xiao 2019/10/12

// MethodInfo 描述方法信息
type MethodInfo struct {
	Name        string          `proto:"order=1,modifier=2"`
	Description string          `proto:"order=2,modifier=1"`
	Parameters  []ParameterInfo `proto:"order=3,modifier=3"`
	ReturnType  ParameterInfo   `proto:"order=4, modifier=1"`
}
