package message

// create by xinlei.xiao 2019/10/12

// ParameterInfo 描述参数信息
type ParameterInfo struct {
	Name        string   `proto:"order=1,modifier=2"`
	Type        TypeInfo `proto:"order=2,modifier=1"`
	Description string   `proto:"order=3,modifier=2"`
}
