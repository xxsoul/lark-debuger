package message

import (
	"bytes"
	"math"
	"reflect"
	"strconv"
	"strings"
	"unsafe"

	"github.com/golang/protobuf/proto"
)

// DecodeInt 从字节流转换回无符号64
func DecodeInt(data []byte) uint64 {
	value, count := proto.DecodeVarint(data)
	if value == 0 && count == 0 {
		return 0
	}
	return value
}

// DecodeString 从字节流转换回string
func DecodeString(data []byte) string {
	return string(data)
}

// DecodeFloat32 从字节流转换回float32
func DecodeFloat32(data []byte) float32 {
	protoBuf := new(proto.Buffer)
	protoBuf.SetBuf(data)

	proVal, _ := protoBuf.DecodeFixed32()
	return float32(math.Float64frombits(proVal))
}

// DecodeFloat64 从字节流转换回float64
func DecodeFloat64(data []byte) float64 {
	protoBuf := new(proto.Buffer)
	protoBuf.SetBuf(data)

	proVal, _ := protoBuf.DecodeFixed32()
	return math.Float64frombits(proVal)
}

// DecodePbMessage 解析字节流并返回给定字节流中第一条protobuf message信息
// msgData:字节流信息，可以包含多条消息，也可以只包含1条消息
// data:protobuf message中包含的纯数据部分
// order:message的序号值
// size:value的长度
// msgSize:整个消息的长度
func DecodePbMessage(msgData []byte) (data []byte, order uint64, tag uint, size uint64, msgSize uint64) {
	var keyIDBuf bytes.Buffer //keyIdBuf key的序列的buffer，用于计算序号
	var valBuf bytes.Buffer   //valBuf 值的二进制字符串缓冲
	var keyTag uint           //key的tag值，表示类型

	msgLen := uint64(0)

	//根据猜想，message中的fieldId字段在超过15时，采用varint编码，这里只要识别到最后1位，取出tag之后右移三位，即可取到正确数字（需要测试）
	for _, b := range msgData {
		msgLen++
		k := b & 0x8f
		if k>>7 == 1 {
			keyIDBuf.WriteByte(b)
		} else {
			keyTag = uint(b & 0x07)
			keyIDBuf.WriteByte(b >> 3)
			break
		}
	}
	keyOrder := DecodeInt(keyIDBuf.Bytes())

	if keyTag == 0 || keyTag == 2 {
		//tag为0或2，后面的部分为varint，按着之前的方式取出对应的byte数组
		newData := msgData[msgLen:]
		for _, b := range newData {
			msgLen++
			valBuf.WriteByte(b)
			if (b&0x8f)>>7 == 0 {
				break
			}
		}
		if keyTag == 0 {
			dataRes := valBuf.Bytes()
			return dataRes, keyOrder, keyTag, uint64(len(dataRes)), msgLen
		}
		//tag为2，后面的部分是varint，表示之后数据的长度
		dataLen := DecodeInt(valBuf.Bytes())
		dataRes := msgData[msgLen : msgLen+dataLen]
		msgLen += dataLen
		return dataRes, keyOrder, keyTag, uint64(len(dataRes)), msgLen
	} else if keyTag == 1 {
		//tag为1，后面部分是64位固定长度
		valBuf.Write(msgData[msgLen : msgLen+8])
		msgLen += 8

		dataRes := valBuf.Bytes()
		return dataRes, keyOrder, keyTag, uint64(len(dataRes)), msgLen
	} else if keyTag == 5 {
		//tag为5，后面部分是32位固定长度
		valBuf.Write(msgData[msgLen : msgLen+4])
		msgLen += 4
		dataRes := valBuf.Bytes()
		return dataRes, keyOrder, keyTag, uint64(len(dataRes)), msgLen
	}
	return nil, 0, 0, 0, 0
}

// protoMessageInfo 结构，用于平行描述一个protoMessage单层结构的类型
type protoMessageInfo struct {
	tag  int
	data []byte
}

// DecodePbMessageToObject 方法，将proto message结构解码为给定的指针类型
// 具体流程是：
/*
1、解析protoMesasge并存入map[int][]byte中
2、遍历目标类型的字段，如果是嵌套类型，则递归，否则，直接设置值
*/
func DecodePbMessageToObject(msgData []byte, resType reflect.Type) interface{} {
	protoMsgMap := decodePbMessageInfo(msgData)
	if protoMsgMap == nil {
		return nil
	}

	// 遍历类型字段
	typeInfo := resType
	typeValue := reflect.New(resType).Elem()
	for i := 0; i < typeInfo.NumField(); i++ {
		fieldType := typeInfo.Field(i)
		fieldValue := typeValue.Field(i)
		fieldTag := fieldType.Tag.Get("proto")

		fOrder := strings.Split(strings.Split(fieldTag, ",")[0], "=")[1] //order
		fModif := strings.Split(strings.Split(fieldTag, ",")[1], "=")[1] //modifier，用来判断是否为数组
		msgInfoList, hasValue := protoMsgMap[stringToUint64(fOrder)]
		if !hasValue || !fieldValue.CanSet() || len(msgInfoList) < 1 {
			continue
		}

		// 这里认为，即使是数组，protoMsg中的tag值不会变
		msgTag := msgInfoList[0].tag
		// 处理数组
		fieldTypeStr := fieldType.Type.String()
		if fModif == "3" || strings.Index(fieldTypeStr, "[]") == 0 && fieldTypeStr != "[]uint8" {
			sliValue := reflect.MakeSlice(fieldType.Type, 0, 0)

			for j := 0; j < len(msgInfoList); j++ {
				msgInfo := msgInfoList[j]
				if len(msgInfo.data) < 1 {
					continue
				}

				if msgTag == 0 {
					if strings.Contains(fieldTypeStr, "bool") {
						sliValue = reflect.Append(sliValue, reflect.ValueOf(DecodeInt(msgInfo.data) > 0))
					} else {
						// 只要不是bool，就无脑转int
						sliValue = reflect.Append(sliValue, reflect.ValueOf(DecodeInt(msgInfo.data)).Convert(fieldType.Type.Elem()))
					}
				} else if msgTag == 1 {
					if strings.Contains(fieldTypeStr, "float") {
						// float全转float64
						sliValue = reflect.Append(sliValue, reflect.ValueOf(DecodeFloat64(msgInfo.data)))
					} else {
						// 无脑转int
						sliValue = reflect.Append(sliValue, reflect.ValueOf(DecodeInt(msgInfo.data)).Convert(fieldType.Type.Elem()))
					}
				} else if msgTag == 5 {
					if strings.Contains(fieldTypeStr, "float") {
						// float全转float32
						sliValue = reflect.Append(sliValue, reflect.ValueOf(DecodeFloat32(msgInfo.data)))
					} else {
						// 无脑转int
						sliValue = reflect.Append(sliValue, reflect.ValueOf(DecodeInt(msgInfo.data)).Convert(fieldType.Type.Elem()))
					}
				} else if msgTag == 2 {
					if fieldTypeStr == "[]string" {
						sliValue = reflect.Append(sliValue, reflect.ValueOf(string(msgInfo.data)))
					} else {
						// 其他的一律认为是嵌套类型
						sliValue = reflect.Append(sliValue, reflect.ValueOf(DecodePbMessageToObject(msgInfo.data, fieldType.Type.Elem())))
					}
				}
			}

			fieldValue.Set(sliValue)
		} else {
			msgInfo := msgInfoList[0]

			if msgInfo.tag == 0 {
				if strings.Contains(fieldTypeStr, "bool") {
					fieldValue.SetBool(DecodeInt(msgInfo.data) > 0)
				} else {
					//只要不是bool，就无脑转int
					fieldKind := fieldValue.Kind()
					if strings.Contains(fieldKind.String(), "u") {
						fieldValue.SetUint(DecodeInt(msgInfo.data))
					} else {
						fieldValue.SetInt(int64(DecodeInt(msgInfo.data)))
					}
				}
				//todo 暂时缺少对枚举的转换，但是枚举的转换本质上也还是varint，所以转换成int也没什么不妥
			} else if msgInfo.tag == 1 {
				if strings.Contains(fieldTypeStr, "float") {
					fieldValue.SetFloat(DecodeFloat64(msgInfo.data))
				} else {
					fieldValue.SetInt(int64(DecodeInt(msgInfo.data)))
				}
			} else if msgInfo.tag == 5 {
				if strings.Contains(fieldTypeStr, "float") {
					fieldValue.SetFloat(float64(DecodeFloat32(msgInfo.data)))
				} else {
					fieldValue.SetInt(int64(DecodeInt(msgInfo.data)))
				}
			} else if msgInfo.tag == 2 {
				if fieldTypeStr == "string" {
					fieldValue.SetString(string(msgInfo.data))
				} else if fieldTypeStr == "[]uint8" {
					fieldValue.SetBytes(msgInfo.data)
				} else {
					//其他类型的，统一按嵌套类型处理
					// decode方法返回的是结构体，怎么转换为指针？
					fieldKind := fieldType.Type.Kind()
					if fieldKind == reflect.Struct {
						fieldValue.Set(reflect.ValueOf(DecodePbMessageToObject(msgInfo.data, fieldType.Type)).Convert(fieldType.Type))
					} else {
						resObj := DecodePbMessageToObject(msgInfo.data, fieldType.Type.Elem())

						// 创建一个指针，并且把interface{}转成指针放到给定的指针上
						fieldValue.Set(reflect.NewAt(fieldType.Type.Elem(), unsafe.Pointer(&resObj)).Convert(fieldType.Type))
					}
				}
			}
		}
	}

	return typeValue.Interface()
}

func stringToUint64(value string) uint64 {
	res, _ := strconv.ParseInt(value, 10, 64)
	return uint64(res)
}

// decodePbMessageInfo 方法，从字节流中，读取单层消息信息并返回
// 考虑到数组的问题，一个映射对一个数组，映射的key为order的值，映射的value为该order下的消息内容
func decodePbMessageInfo(msgData []byte) map[uint64][]protoMessageInfo {
	offest := 0
	msgInfoMap := make(map[uint64][]protoMessageInfo, 0)

	for offest < len(msgData) {
		d, o, t, _, ms := DecodePbMessage(msgData[offest:])
		offest += int(ms)

		msgInfo := protoMessageInfo{
			tag:  int(t),
			data: d,
		}

		if msgInfoMap[o] == nil {
			msgInfoMap[o] = make([]protoMessageInfo, 0)
		}
		msgInfoMap[o] = append(msgInfoMap[o], msgInfo)
	}
	if len(msgInfoMap) > 0 {
		return msgInfoMap
	}
	return nil
}
