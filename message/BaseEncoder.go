package message

import (
	"bytes"
	"math"
	"reflect"
	"strings"

	"github.com/golang/protobuf/proto"
)

// EncodeInt 编码int值
func EncodeInt(order int, value uint64) []byte {
	var buf bytes.Buffer

	valueTag := encodeTag(order, 0x00)
	valueByte := proto.EncodeVarint(value)
	buf.Write(valueTag)
	buf.Write(valueByte)

	// fmt.Println("buffer data " + strconv.Itoa(order) + " is " + fmt.Sprintf("%b", buf.Bytes()))
	return buf.Bytes()
}

// EncodeFloat32 编码float32值
func EncodeFloat32(order int, value float32) []byte {
	var buf bytes.Buffer
	proBuf := new(proto.Buffer)

	valueTag := encodeTag(order, 0x05)

	proBuf.EncodeFixed32(uint64(math.Float32bits(value)))
	valueByte := proBuf.Bytes()
	// binary.LittleEndian.PutUint32(valueByte, math.Float32bits(value))

	buf.Write(valueTag)
	buf.Write(valueByte)

	return buf.Bytes()
}

// EncodeFloat64 编码float64值
func EncodeFloat64(order int, value float64) []byte {
	var buf bytes.Buffer
	proBuf := new(proto.Buffer)

	valueTag := encodeTag(order, 0x01)

	proBuf.EncodeFixed64(math.Float64bits(value))
	valueByte := proBuf.Bytes()
	// binary.LittleEndian.PutUint64(valueByte, math.Float64bits(value))

	buf.Write(valueTag)
	buf.Write(valueByte)

	return buf.Bytes()
}

// EncodeString 编码string值
func EncodeString(order int, value string) []byte {
	return EncodeByte(order, []byte(value))
}

// EncodeByte 编码byte字节流
func EncodeByte(order int, value []byte) []byte {
	var buf bytes.Buffer

	valueTag := encodeTag(order, 0x02)
	valueByte := value
	buf.Write(valueTag)
	buf.Write(proto.EncodeVarint(uint64(len(valueByte))))
	buf.Write(valueByte)

	// fmt.Println("buffer data " + strconv.Itoa(order) + " is " + fmt.Sprintf("%b", buf.Bytes()))
	return buf.Bytes()
}

// EncodeObjectToPbMessage 把给定对象编码为proto message字节流
func EncodeObjectToPbMessage(obj interface{}) (data []byte, result bool) {
	if obj == nil {
		return nil, false
	}

	objKind := reflect.ValueOf(obj).Kind()
	var typeValue reflect.Value
	var typeInfo reflect.Type

	if objKind == reflect.Ptr {
		typeValue = reflect.ValueOf(obj).Elem()
		typeInfo = typeValue.Type()
	} else if objKind == reflect.Struct {
		typeValue = reflect.ValueOf(obj)
		typeInfo = reflect.TypeOf(obj)
	} else {
		return nil, false
	}

	resBuf := new(bytes.Buffer)
	for i := 0; i < typeInfo.NumField(); i++ {
		fieldType := typeInfo.Field(i)
		fieldTypeStr := fieldType.Type.String()
		fieldValue := typeValue.Field(i)
		fieldTag := fieldType.Tag.Get("proto")

		if len(fieldTag) < 1 {
			continue
		}

		fOrder := strings.Split(strings.Split(fieldTag, ",")[0], "=")[1] //order
		fModif := strings.Split(strings.Split(fieldTag, ",")[1], "=")[1] //modifier，用来判断是否为数组

		if fModif == "3" || strings.Index(fieldTypeStr, "[]") == 0 && fieldTypeStr != "[]uint8" {
			for j := 0; j < fieldValue.Len(); j++ {
				var sliValue reflect.Value
				if fieldValue.Kind() == reflect.Ptr {
					sliValue = fieldValue.Index(j).Elem()
				} else {
					sliValue = fieldValue.Index(j)
				}

				resBuf.Write(encodeReflectValue(stringToUint64(fOrder), sliValue))
			}
		} else {
			resBuf.Write(encodeReflectValue(stringToUint64(fOrder), fieldValue))
		}
	}

	return resBuf.Bytes(), true
}

func encodeReflectValue(order uint64, objValue reflect.Value) []byte {
	sliTypeStr := objValue.Type().String()

	if sliTypeStr == "bool" {
		if objValue.Bool() {
			return EncodeInt(int(order), 1)
		}
		return EncodeInt(int(order), 0)
	} else if strings.Contains(sliTypeStr, "int") && !strings.Contains(sliTypeStr, "uint8") {
		// 带int字符按int处理
		return EncodeInt(int(order), uint64(objValue.Int()))
	} else if sliTypeStr == "float32" {
		return EncodeFloat32(int(order), float32(objValue.Float()))
	} else if sliTypeStr == "float64" {
		return EncodeFloat64(int(order), objValue.Float())
	} else if sliTypeStr == "string" {
		return EncodeString(int(order), objValue.String())
	} else if sliTypeStr == "uint8" || sliTypeStr == "[]uint8" {
		return EncodeByte(int(order), objValue.Bytes())
	}

	var nestValue reflect.Value
	if objValue.Kind() == reflect.Struct {
		nestValue = objValue
	} else {
		nestValue = objValue.Elem()
	}
	resByt, res := EncodeObjectToPbMessage(nestValue.Interface())

	if res {
		return EncodeByte(int(order), resByt)
	}
	return nil
}

// 编码MessageTag部分
func encodeTag(tagOrder int, tagType byte) []byte {
	orderByte := proto.EncodeVarint(uint64(tagOrder))
	lastByte := orderByte[len(orderByte)-1]

	//最后一个元素左移3位，对tagType取或
	orderByte[len(orderByte)-1] = lastByte<<3 | tagType
	return orderByte
}

func stringToInt(value string) int {
	return int(stringToUint64(value))
}
